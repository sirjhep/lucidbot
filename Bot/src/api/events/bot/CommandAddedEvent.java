package api.events.bot;

import api.commands.Command;
import lombok.AllArgsConstructor;
import lombok.Getter;
import spi.commands.CommandHandlerFactory;

import javax.annotation.Nullable;

@Getter
@AllArgsConstructor
public class CommandAddedEvent {
    private final CommandHandlerFactory commandHandlerFactory;
    @Nullable
    private final Command replaced;
}
