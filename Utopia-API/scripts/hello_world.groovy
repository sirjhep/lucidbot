import api.commands.*

def Command handles() {
    return CommandBuilder.forCommand("groovy").build();
}

def CommandResponse handleCommand(context, params, filters, delayedEventPoster) {
    return CommandResponse.resultResponse("helloWorld", "Hello world from Groovy");
}

def CommandParser[] getParsers() {
    def parsers = new CommandParser[2];
    parsers[0] = new CommandParser(new ParamParsingSpecification("name", "[^ ]+"));
    parsers[1] = CommandParser.getEmptyParser();
    return parsers;
}