package database.models;

import api.filters.FilterEnabled;
import filtering.filters.AgeFilter;
import lombok.*;
import org.hibernate.annotations.Sort;
import org.hibernate.annotations.SortType;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "poll")
@NoArgsConstructor
@EqualsAndHashCode(of = {"description"})
@Getter
@Setter
public class Poll implements Comparable<Poll> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    @Setter(AccessLevel.NONE)
    private Long id;

    @Lob
    @Column(name = "description", nullable = false, length = 1000)
    private String description;

    @OneToMany(mappedBy = "poll", cascade = CascadeType.ALL, orphanRemoval = true)
    @Sort(type = SortType.NATURAL)
    @Setter(AccessLevel.NONE)
    private List<PollOption> pollOptions = new ArrayList<>();

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "bindings_id", updatable = false, nullable = false, unique = true)
    private Bindings bindings;

    @Column(name = "added_by", updatable = false, nullable = false, length = 200)
    private String addedBy;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "added", updatable = false, nullable = false)
    private Date added;

    @Column(name = "started", nullable = false)
    private boolean started;

    @Column(name = "closed", nullable = false)
    private boolean closed;

    public Poll(final String description, final Bindings bindings, final String addedBy) {
        this.description = description;
        this.bindings = bindings;
        this.addedBy = addedBy;
        this.added = new Date();
    }

    public boolean addPollOption(final PollOption pollOption) {
        if (!pollOptions.contains(pollOption) && !started && !closed) {
            pollOptions.add(pollOption);
            return true;
        }
        return false;
    }

    @FilterEnabled(AgeFilter.class)
    public Date getAdded() {
        return new Date(added.getTime());
    }

    @Override
    public int compareTo(final Poll o) {
        return added.compareTo(o.getAdded());
    }
}
