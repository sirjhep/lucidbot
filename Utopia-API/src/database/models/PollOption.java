package database.models;

import api.database.models.BotUser;
import lombok.*;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

@Entity
@Table(name = "poll_option", uniqueConstraints = @UniqueConstraint(columnNames = {"description", "poll_id"}))
@NoArgsConstructor
@EqualsAndHashCode(of = {"description", "poll"})
@Getter
@Setter
public class PollOption implements Comparable<PollOption> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    @Setter(AccessLevel.NONE)
    private Long id;

    @Lob
    @Column(name = "description", nullable = false, length = 1000)
    private String description;

    @ManyToOne(optional = false)
    @JoinColumn(name = "poll_id", nullable = false, updatable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Poll poll;

    @OneToMany(mappedBy = "pollOption", cascade = CascadeType.ALL, orphanRemoval = true)
    @Setter(AccessLevel.NONE)
    private Set<PollVote> pollVotes = new HashSet<>();

    public PollOption(final String description, final Poll poll) {
        this.description = description;
        this.poll = poll;
    }

    public boolean addVote(final PollVote vote) {
        return pollVotes.add(vote);
    }

    public void removeVote(final BotUser user) {
        Iterator<PollVote> iterator = pollVotes.iterator();
        while (iterator.hasNext()) {
            PollVote next = iterator.next();
            if (next.getUser().equals(user)) {
                iterator.remove();
                break;
            }
        }
    }

    @Override
    public int compareTo(final PollOption o) {
        return id.compareTo(o.getId());
    }
}
