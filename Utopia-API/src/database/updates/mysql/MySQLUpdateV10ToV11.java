package database.updates.mysql;

import api.database.updates.DatabaseUpdateAction;
import api.database.updates.SimpleUpdateAction;
import com.google.common.collect.Lists;
import lombok.extern.log4j.Log4j;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;

@Log4j
public class MySQLUpdateV10ToV11 extends ApiMySQLDatabaseUpdater {
    @Override
    public int updatesToVersion() {
        return 11;
    }

    @Override
    public Iterable<? extends DatabaseUpdateAction> getUpdateActions() {
        return Lists.newArrayList(
                new SimpleUpdateAction("ALTER TABLE user_activities ADD last_polls_check DATETIME NOT NULL"),
                new DatabaseUpdateAction() {
                    @Override
                    public void runDatabaseAction(final Connection connection) throws SQLException {
                        try (PreparedStatement preparedStatement = connection.prepareStatement("UPDATE user_activities SET last_polls_check = ?")) {
                            preparedStatement.setDate(1, new Date(System.currentTimeMillis()));
                            preparedStatement.executeUpdate();
                        } catch (SQLException e) {
                            MySQLUpdateV10ToV11.log.error("", e);
                        }
                    }
                }
        );
    }
}
