package database.updates.mysql;

import api.database.updates.DatabaseUpdateAction;
import api.database.updates.SimpleUpdateAction;
import com.google.common.collect.Lists;
import lombok.extern.log4j.Log4j;

@Log4j
public class MySQLUpdateV11ToV12 extends ApiMySQLDatabaseUpdater {
    @Override
    public int updatesToVersion() {
        return 12;
    }

    @Override
    public Iterable<? extends DatabaseUpdateAction> getUpdateActions() {
        return Lists.newArrayList(
                new SimpleUpdateAction("UPDATE bonus SET bonus_value = 0.05 WHERE name = 'Dwarf Gains'"),
                new SimpleUpdateAction("UPDATE race SET elite_def_strength = 1, elite_networth = 4.75 WHERE name = 'Dwarf'"),
                new SimpleUpdateAction("UPDATE race SET def_spec_strength = 5, elite_networth = 5.25 WHERE name = 'Elf'"),
                new SimpleUpdateAction("UPDATE race SET off_spec_strength = 5 WHERE name = 'Halfling'")
        );
    }
}
