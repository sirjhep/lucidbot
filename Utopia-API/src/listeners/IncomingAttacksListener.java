package listeners;

import api.database.models.BotUser;
import api.database.transactions.SimpleTransactionTask;
import api.events.DelayedEventPoster;
import api.runtime.ThreadingManager;
import api.settings.PropertiesCollection;
import com.google.common.collect.Multimap;
import com.google.common.eventbus.Subscribe;
import database.daos.NotificationDAO;
import database.daos.ProvinceDAO;
import database.models.NewsItem;
import database.models.Notification;
import database.models.NotificationType;
import database.models.Province;
import events.IncomingAttacksEvent;
import lombok.extern.log4j.Log4j;
import spi.events.EventListener;
import tools.communication.NotificationDeliverer;

import javax.inject.Inject;
import javax.inject.Provider;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import static api.database.transactions.Transactions.inTransaction;
import static api.tools.time.DateUtil.isBefore;
import static api.tools.time.DateUtil.minutesToMillis;
import static tools.UtopiaPropertiesConfig.*;

@Log4j
public class IncomingAttacksListener implements EventListener {

    private final Provider<ProvinceDAO> provinceDAOProvider;
    private final Provider<NotificationDAO> notificationDAOProvider;
    private final Provider<NotificationDeliverer> delivererProvider;
    private final ThreadingManager threadingManager;
    private final PropertiesCollection properties;

    private final ConcurrentMap<Long, String> usersLastAnnouncedAttack = new ConcurrentHashMap<>();
    private final ConcurrentMap<Long, Date> usersLastAnnouncedAttackTime = new ConcurrentHashMap<>();

    @Inject
    public IncomingAttacksListener(final Provider<ProvinceDAO> provinceDAOProvider,
                                   final Provider<NotificationDAO> notificationDAOProvider,
                                   final Provider<NotificationDeliverer> delivererProvider,
                                   final ThreadingManager threadingManager,
                                   final PropertiesCollection properties) {
        this.provinceDAOProvider = provinceDAOProvider;
        this.notificationDAOProvider = notificationDAOProvider;
        this.delivererProvider = delivererProvider;
        this.threadingManager = threadingManager;
        this.properties = properties;
    }

    @Subscribe
    public void onIncomingAttacks(final IncomingAttacksEvent event) {
        threadingManager.execute(new Runnable() {
            @Override
            public void run() {
                inTransaction(new SimpleTransactionTask() {
                    @Override
                    public void run(final DelayedEventPoster delayedEventPoster) {
                        try {
                            List<Notification> notifications = notificationDAOProvider.get().getNotifications(NotificationType.INCOMING_ATTACK);
                            if (notifications.isEmpty()) return;

                            ProvinceDAO provinceDAO = provinceDAOProvider.get();
                            NotificationDeliverer notificationDeliverer = delivererProvider.get();
                            Multimap<String, NewsItem> incomingAttacksPerProvince = event.getIncomingAttacksPerProvince();

                            deliverNotifications(notifications, provinceDAO, notificationDeliverer, incomingAttacksPerProvince);
                        } catch (Exception e) {
                            IncomingAttacksListener.log.error("", e);
                        }
                    }
                });
            }
        });
    }

    private void deliverNotifications(final List<Notification> notifications,
                                      final ProvinceDAO provinceDAO,
                                      final NotificationDeliverer notificationDeliverer,
                                      final Multimap<String, NewsItem> incomingAttacksPerProvince) {
        for (Notification notification : notifications) {
            BotUser user = notification.getUser();
            Province province = provinceDAO.getProvinceForUser(user);
            if (province != null && incomingAttacksPerProvince.containsKey(province.getName())) {
                Deque<NewsItem> attacksToAnnounce = getAttacksToAnnounce(province.getId(), incomingAttacksPerProvince.get(province.getName()));
                if (!attacksToAnnounce.isEmpty()) {
                    String message = notification.getMethod().hasMessageLengthLimit() ?
                            "In recent news posted you've been hit " + attacksToAnnounce.size() + " times" :
                            "Recently posted news affecting your province:\n" + mergeNewsItems(attacksToAnnounce);
                    notification.getMethod().deliver(notificationDeliverer, user, "Incoming attacks", message);

                    usersLastAnnouncedAttack.put(province.getId(), attacksToAnnounce.getLast().getOriginalMessage());
                    usersLastAnnouncedAttackTime.put(province.getId(), new Date());
                }
            }
        }
    }

    private Deque<NewsItem> getAttacksToAnnounce(final long provinceId, final Collection<NewsItem> newsItems) {
        Date lastAnnouncement = usersLastAnnouncedAttackTime.get(provinceId);
        if (currentTimeIsBeforeEarliestAllowedNextAnnouncement(lastAnnouncement))
            return new LinkedList<>();

        NavigableSet<NewsItem> sortedNewsItems = getNewsItemsSorted(newsItems);

        String lastAnnouncedAttack = usersLastAnnouncedAttack.get(provinceId);
        Date lastAnnouncedTick = toNearestTick(lastAnnouncement);
        Date earliestNewsIncludeDate = getEarliestNewsIncludeDate();

        Deque<NewsItem> toAnnounce = new LinkedList<>();
        for (NewsItem newsItem : sortedNewsItems.descendingSet()) {
            if (isFromBeforeLastAnnouncement(newsItem, lastAnnouncedTick) ||
                    isTooOldToBeAnnounced(newsItem, earliestNewsIncludeDate) ||
                    isTheLastAnnouncedAttack(newsItem, lastAnnouncedAttack))
                break;
            toAnnounce.addFirst(newsItem);
        }

        return toAnnounce;
    }

    private boolean currentTimeIsBeforeEarliestAllowedNextAnnouncement(final Date lastAnnouncement) {
        int minMinutesBetweenAnnouncements = properties.getInteger(INCOMING_ATTACKS_NOTIFICATION_MIN_INTERVAL);
        return lastAnnouncement != null &&
                lastAnnouncement.getTime() + minutesToMillis(minMinutesBetweenAnnouncements) > System.currentTimeMillis();
    }

    private static NavigableSet<NewsItem> getNewsItemsSorted(final Collection<NewsItem> newsItems) {
        NavigableSet<NewsItem> sortedNewsItems = new TreeSet<>(new Comparator<NewsItem>() {
            @Override
            public int compare(final NewsItem o1, final NewsItem o2) {
                return o1.getRealDate().compareTo(o2.getRealDate());
            }
        });
        sortedNewsItems.addAll(newsItems);
        return sortedNewsItems;
    }

    private Date toNearestTick(final Date date) {
        int tickLengthInMinutes = properties.getInteger(TICK_LENGTH);
        return date == null ? null : new Date(date.getTime() - (date.getTime() % minutesToMillis(tickLengthInMinutes)));
    }

    private Date getEarliestNewsIncludeDate() {
        int tickLengthInMinutes = properties.getInteger(TICK_LENGTH);
        int maxAgeInTicks = properties.getInteger(INCOMING_ATTACKS_NOTIFICATION_MAX_AGE);
        Date lastTick = toNearestTick(new Date());
        return new Date(lastTick.getTime() - minutesToMillis(tickLengthInMinutes * maxAgeInTicks));
    }

    private static boolean isFromBeforeLastAnnouncement(final NewsItem newsItem, final Date lastAnnouncedTick) {
        return lastAnnouncedTick != null && isTooOldToBeAnnounced(newsItem, lastAnnouncedTick);
    }

    private static boolean isTooOldToBeAnnounced(final NewsItem newsItem, final Date maxAgeDate) {
        return isBefore(newsItem.getRealDate(), maxAgeDate);
    }

    private static boolean isTheLastAnnouncedAttack(final NewsItem newsItem, final String lastAnnouncedAttack) {
        return newsItem.getOriginalMessage().equals(lastAnnouncedAttack);
    }

    private static String mergeNewsItems(final Collection<NewsItem> newsItems) {
        StringBuilder builder = new StringBuilder(200);
        for (NewsItem newsItem : newsItems) {
            builder.append(newsItem.getOriginalMessage()).append('\n');
        }
        return builder.toString().trim();
    }

}
