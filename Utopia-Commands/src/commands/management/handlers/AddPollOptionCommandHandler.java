package commands.management.handlers;

import api.commands.CommandHandlingException;
import api.commands.CommandResponse;
import api.events.DelayedEventPoster;
import api.runtime.IRCContext;
import api.tools.collections.Params;
import database.daos.PollDAO;
import database.models.Poll;
import database.models.PollOption;
import spi.commands.CommandHandler;
import spi.filters.Filter;

import javax.inject.Inject;
import java.util.Collection;

public class AddPollOptionCommandHandler implements CommandHandler {

    private final PollDAO pollDAO;

    @Inject
    public AddPollOptionCommandHandler(final PollDAO pollDAO) {
        this.pollDAO = pollDAO;
    }

    @Override
    public CommandResponse handleCommand(final IRCContext context,
                                         final Params params,
                                         final Collection<Filter<?>> filters,
                                         final DelayedEventPoster delayedEventPoster) throws CommandHandlingException {
        try {
            Poll poll = pollDAO.getPoll(params.getLongParameter("id"));
            if (poll == null) return CommandResponse.errorResponse("No such poll");

            String description = params.getParameter("description");
            PollOption pollOption = new PollOption(description, poll);
            boolean added = poll.addPollOption(pollOption);
            if (!added) return CommandResponse.errorResponse("Option already exists");

            pollDAO.save(poll);
            return CommandResponse.resultResponse("pollOption", pollOption);
        } catch (final Exception e) {
            throw new CommandHandlingException(e);
        }
    }

}
