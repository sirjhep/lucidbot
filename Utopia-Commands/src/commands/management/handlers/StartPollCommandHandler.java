package commands.management.handlers;

import api.commands.CommandHandlingException;
import api.commands.CommandResponse;
import api.events.DelayedEventPoster;
import api.runtime.IRCContext;
import api.tools.collections.Params;
import database.daos.PollDAO;
import database.models.Poll;
import events.PollStartedEvent;
import spi.commands.CommandHandler;
import spi.filters.Filter;

import javax.inject.Inject;
import java.util.Collection;

public class StartPollCommandHandler implements CommandHandler {

    private final PollDAO pollDAO;

    @Inject
    public StartPollCommandHandler(final PollDAO pollDAO) {
        this.pollDAO = pollDAO;
    }

    @Override
    public CommandResponse handleCommand(final IRCContext context,
                                         final Params params,
                                         final Collection<Filter<?>> filters,
                                         final DelayedEventPoster delayedEventPoster) throws CommandHandlingException {
        try {
            Poll poll = pollDAO.getPoll(params.getLongParameter("id"));
            if (poll == null) return CommandResponse.errorResponse("No such poll");

            poll.setStarted(true);
            poll = pollDAO.save(poll);

            delayedEventPoster.enqueue(new PollStartedEvent(poll.getId(), context));

            return CommandResponse.resultResponse("poll", poll);
        } catch (final Exception e) {
            throw new CommandHandlingException(e);
        }
    }
}
