<@ircmessage type="reply_notice">
[id:${poll.id} - <#if poll.closed>${RED}closed${NORMAL}<#else>${GREEN}open${NORMAL}</#if>]: <#if poll.closed>${DARK_GRAY}<#else>${RED}</#if>${poll.description+NORMAL}
Added ${DARK_GRAY+timeUtil.compareDateToCurrent(poll.added)+NORMAL} ago by ${DARK_GRAY+poll.addedBy+NORMAL}
Voting options:
    <#list poll.pollOptions as option>
    ${option_index}. ${OLIVE+option.description+NORMAL}
        <@compact intro=DARK_GREEN+option.pollVotes?size+" votes: "+NORMAL>
            <#list option.pollVotes as vote>
            ${BLUE+vote.user.mainNick+NORMAL}
            </#list>
        </@compact>
    </#list>
</@ircmessage>