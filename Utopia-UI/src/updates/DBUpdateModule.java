package updates;

import api.database.updates.DatabaseUpdater;
import com.google.inject.AbstractModule;
import com.google.inject.multibindings.Multibinder;

public class DBUpdateModule extends AbstractModule {

    @Override
    protected void configure() {
        Multibinder<DatabaseUpdater> binder = Multibinder.newSetBinder(binder(), DatabaseUpdater.class);
    }

}
