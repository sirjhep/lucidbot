package updates.mysql;

import api.database.updates.AbstractMySQLDatabaseUpdater;

public abstract class UiMySQLDatabaseUpdater extends AbstractMySQLDatabaseUpdater {

    @Override
    public String forArtifact() {
        return "Utopia-UI";
    }

}
