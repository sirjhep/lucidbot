package web.models;

import database.models.PollOption;
import database.models.PollVote;
import org.hibernate.validator.constraints.NotEmpty;
import web.validation.Add;
import web.validation.Update;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@XmlRootElement(name = "PollOption")
@XmlAccessorType(XmlAccessType.FIELD)
public class RS_PollOption {
    /**
     * The id for this poll option. The id is set by the database, so clients will only use it in the URL's.
     * <p/>
     * The server will simply ignore this value if you send it in with some request.
     */
    @XmlElement(name = "ID")
    private Long id;

    /**
     * The poll option description.
     * <p/>
     * This value is updatable, so it will always be overwritten when you do update calls. I.e. it always has to be specified, regardless of it's an
     * update or add request.
     */
    @NotEmpty(message = "The poll option description may not be null or empty", groups = {Add.class, Update.class})
    @XmlElement(required = true, name = "Description")
    private String description;

    /**
     * The votes for this option. For display purposes only.
     * <p/>
     * The server will simply ignore this value if you send it in with some request.
     */
    @XmlElementWrapper(name = "Votes")
    @XmlElement(name = "Vote")
    private List<String> votes;

    public RS_PollOption() {
    }

    public RS_PollOption(final Long id, final String description, final List<String> votes) {
        this.id = id;
        this.description = description;
        this.votes = votes;
    }

    public static RS_PollOption fromPollOption(final PollOption pollOption) {
        List<String> votes = new ArrayList<>(pollOption.getPollVotes().size());
        for (PollVote vote : pollOption.getPollVotes()) {
            votes.add(vote.getUser().getMainNick());
        }
        Collections.sort(votes);
        return new RS_PollOption(pollOption.getId(), pollOption.getDescription(), votes);
    }

    public Long getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public List<String> getVotes() {
        return votes;
    }
}
