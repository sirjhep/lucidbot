package web.resources;

import api.database.daos.BotUserDAO;
import api.database.models.BotUser;
import api.database.transactions.Transactional;
import api.tools.validation.ValidationEnabled;
import database.daos.ProvinceDAO;
import database.models.Province;
import tools.parsing.AttackParser;
import web.documentation.Documentation;
import web.tools.WebContext;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import static com.google.common.base.Preconditions.checkArgument;

@ValidationEnabled
@Path("attacking")
public class AttackingResource {
    private final AttackParser attackParser;
    private final BotUserDAO botUserDAO;
    private final ProvinceDAO provinceDAO;

    @Inject
    public AttackingResource(final AttackParser attackParser, final BotUserDAO botUserDAO, final ProvinceDAO provinceDAO) {
        this.attackParser = attackParser;
        this.botUserDAO = botUserDAO;
        this.provinceDAO = provinceDAO;
    }

    @Documentation("Used for submitting attacks. Takes unparsed attack messages from inside the game")
    @POST
    @Consumes({MediaType.TEXT_PLAIN})
    @Transactional
    public void addAttack(@Documentation(value = "The unparsed attack to add", itemName = "attack")
                          final String attack,
                          @Context
                          final WebContext context) {
        BotUser user = botUserDAO.getUser(context.getName());
        Province attacker = provinceDAO.getProvinceForUser(user);

        checkArgument(attacker != null, "You can't register attacks when you don't have a province yourself");

        attackParser.parse(attack);
    }

}
